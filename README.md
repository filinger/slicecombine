# What is it? #
Test project for the job interview at *ArPoint*. The goal was to make a small Unity3D editor utility that allows to combine image slices into one big texture.

# Usage #
Usage is simple: just select few images in the project assets view, then go to **Image**->**Combine Slices**, validate selected images and press **Combine!**, wait a bit for the process to finish and you will be prompted with **Save File** dialog.