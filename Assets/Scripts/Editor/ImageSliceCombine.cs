﻿#region

using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

#endregion

public class ImageSliceCombile : ScriptableWizard {
    public Texture2D[] imageSlices;

    private const int maxAtlasSize = 8192;

    [MenuItem("Image/Combine Slices")]
    private static void CreateWindow() {
        DisplayWizard("Combine image slices", typeof (ImageSliceCombile), "Combine!");
    }

    private void OnWizardUpdate() {
        imageSlices = Selection.objects.Where(item => item is Texture2D).Cast<Texture2D>().OrderBy(texture => texture.name).ToArray();
        helpString = "Combines image slices into one big image";
        if (imageSlices == null || imageSlices.Length < 1) {
            errorString = string.Format("Please select at least one image");
            isValid = false;
        } else {
            errorString = "";
            isValid = true;
        }
    }

    private void OnWizardCreate() {
        SaveTexture(CombineUsingPack(imageSlices));
    }

    private Texture2D CombineUsingPack(Texture2D[] textures) {
        Texture2D combined = new Texture2D(maxAtlasSize, maxAtlasSize, TextureFormat.ARGB32, false);
        combined.PackTextures(textures, 0, maxAtlasSize);
        combined.name = "combined";
        return combined;
    }

    private void SaveTexture(Texture2D texture) {
        string path = EditorUtility.SaveFilePanel("Save texture as PNG", "", texture.name + ".png", "png");
        if (path.Length != 0) {
            byte[] pngData = ConvertToPNGCompatible(texture).EncodeToPNG();
            if (pngData != null) {
                File.WriteAllBytes(path, pngData);
            }
        }
    }

    private Texture2D ConvertToPNGCompatible(Texture2D texture) {
        if (texture.format == TextureFormat.ARGB32 || texture.format == TextureFormat.RGB24) {
            return texture;
        }
        Texture2D compatible = new Texture2D(texture.width, texture.height);
        compatible.SetPixels(texture.GetPixels(0), 0);
        return compatible;
    }
}
